$(function () {
    $(document).on('click', '.card, .card-caption__link', function () {
        var el = $(this).closest('.card-item');
        el.toggleClass('card-item--selected');
        if(el.hasClass('card-item--selected-hover')){
            el.removeClass('card-item--selected-hover');
        }
        return false;
    });
    $('.card-item').on('mouseenter', function () {
        if (!$(this).hasClass('card-item--disabled')) {
            if ($(this).hasClass('card-item--selected')) {

                $(this).addClass('card-item--selected-hover');

            } else {
                $(this).addClass('card-item--hover');
            }
        }
    });
    $('.card-item').on('mouseleave', function () {
        $(this).removeClass('card-item--hover');
        $(this).removeClass('card-item--selected-hover');
    });
});